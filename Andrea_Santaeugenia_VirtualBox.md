# Exercicis
1. Crea les màquines virtuals plantilla de Ubuntu.
![](m11.virtualbox/capturas/1.png)

2. Crea un clon enllaçat de l'Ubuntu Desktop i activa la xarxa "només Amfitrió". Posa'l en marxa.
![](m11.virtualbox/capturas/1,5.png)
![](m11.virtualbox/capturas/2.png)

3. Mira quina adreça té l'equip convidat i quina adreça s'ha assignat a l'equip amfitrió a la interfície vobxnet0.
>Aquí crec que m'he equivocat perque he agafat la maquina amfitriona el ubuntu server i com a invitat la maquina ubuntu desk. I he fet tots els pasos des d'aquí, no se si està bé o no.>

![](m11.virtualbox/capturas/3.png)

4. Fes ping des de la màquina amfitriona a la màquina convidada i des de la convidada a la amfitriona.
![](m11.virtualbox/capturas/4.png)
![](m11.virtualbox/capturas/5.png)

5. Connecta't via ssh a la màquina convidada des de l'amfitriona.
![](m11.virtualbox/capturas/6.png)

6. Clona un ubuntu server i activa la xarxa NAT. Posa'l en marxa. Comprova que tens accés a Internet.
![](m11.virtualbox/capturas/8.png)

7. Instal·la el servei ssh al server.
![](m11.virtualbox/capturas/9.png)

8. Configura el reenviament de ports de la màquina virtual creada i posa 2222 al port amfitrió i 22 al port convidat.
![](m11.virtualbox/capturas/7.png)

9. Connecta via ssh al servidor.
![](m11.virtualbox/capturas/5.png)
